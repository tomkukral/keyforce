package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/spf13/viper"
)

type config struct {
	// URL with keys
	Sources []string

	// Target is authorized_keys file
	Target string

	// Defines sleep interval
	SleepSec int
}

var c config

func init() {
	viper.SetConfigName("keyforce")
	viper.AddConfigPath("/etc/keyforce")
	viper.AddConfigPath(".")

	viper.SetDefault("target", "/root/.ssh/authorized_keys")
	viper.SetDefault("sleepSec", 600)

	err := viper.ReadInConfig()
	if err != nil {
		log.Printf("Unable to read config file, using defaults: %s", err)
	}

	viper.Unmarshal(&c)

	if c.Sources == nil {
		c.Sources = []string{
			"https://github.com/tomkukral.keys",
		}
	}
}

var rem = regexp.MustCompile("(?m)^ssh-.*")

func enforce() error {
	log.Println("SSH key enforicing started")

	keys := []string{}

	for _, url := range c.Sources {
		log.Printf("Reading keys from %s", url)
		resp, err := http.Get(url)
		if err != nil {
			log.Printf("Unable to fetch keys from %s", url)
			continue
		}
		if resp.StatusCode != http.StatusOK {
			log.Printf("Got status code %d on %s", resp.StatusCode, url)
			continue
		}

		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Printf("Unable to read body: %s", err)
		}
		bs := string(body)

		// read lines and match ssh key
		for _, line := range strings.Split(bs, "\n") {
			if rem.MatchString(line) {
				keys = append(keys, fmt.Sprintf("%s %s", line, url))
			}
		}

	}

	// enforce keys in target file
	dat, err := ioutil.ReadFile(c.Target)
	if err != nil {
		log.Printf("Unable to read target: %s", err)
	}
	fc := string(dat)

	// ensure key is in fc
	for _, key := range keys {
		m := regexp.MustCompile(fmt.Sprintf("(?m)^%s", strings.Replace(key, "+", "\\+", -1)))
		fmt.Println(m)

		// add to fc if missing
		if !m.MatchString(fc) {
			log.Printf("Adding key: %s", key)
			fc = fmt.Sprintf("%s\n%s", key, fc)
		}
	}

	// write output file
	fcc := []byte(fc)
	err = ioutil.WriteFile(c.Target, fcc, 0600)
	if err != nil {
		return fmt.Errorf("Unable to write target file: %s", err)
	}
	log.Printf("Keys added to %s", c.Target)

	return nil
}

func main() {
	for {
		err := enforce()
		if err != nil {
			log.Printf("Failed to enfoce keys: %s", err)
		}
		time.Sleep(time.Duration(c.SleepSec) * time.Second)
	}
}
